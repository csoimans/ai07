CREATE SCHEMA transformationDevoir;

/*
La table des categories est très propre : tout est renseigné et déjà au bon format.
Je change le type dès maintenant.
*/
CREATE TABLE transformationDevoir.categorie(
  categorieDefautCode char(1),
  categorieDefautLong varchar
);

INSERT INTO transformationDevoir.categorie
	SELECT CAST(categorieDefautCode AS CHAR),
         CAST(categorieDefautLong AS VARCHAR)
  FROM extractionDevoir.categorie;

INSERT INTO transformationDevoir.categorie VALUES ('U', 'Unknown');
/*------------------------------------------------------------------------------*/

/*
Pareillement, cette table semble propre : pas de données nulles, toujours les
bons formats.
Je change le type dès maintenant.
*/

CREATE TABLE transformationDevoir.defautType(
  numDefaut integer,
  categorieDefautCode char(1)
);

INSERT INTO transformationDevoir.defautType
	SELECT CAST(numDefaut AS INTEGER),
         CAST(categorieDefautCode AS CHAR)
  FROM extractionDevoir.defautType;

INSERT INTO transformationDevoir.defautType VALUES (0, 'U');
/*------------------------------------------------------------------------------*/

CREATE TABLE transformationDevoir.departement(
  numDepartement integer,
  nomDepartement varchar,
  populationDepartement integer
);

INSERT INTO transformationDevoir.departement
  SELECT CAST(numDepartement AS INTEGER),
         CAST(nomDepartement AS VARCHAR),
         CAST(populationDepartement AS INTEGER)
  FROM extractionDevoir.departement;

/*------------------------------------------------------------------------------*/

CREATE TABLE transformationDevoir.usine(
  numUsine integer,
  anneeCreation integer,
  ageUsine integer,
  capaciteProduction integer,
  numDepartement integer
);

INSERT INTO transformationDevoir.usine
  SELECT CAST(numUsine AS INTEGER),
         CAST(anneeCreation AS INTEGER),
         CAST(ageUsine AS INTEGER),
         CAST(capaciteProduction AS INTEGER),
         CAST(numDepartement AS INTEGER)
  FROM extractionDevoir.usine;


INSERT INTO transformationDevoir.usine VALUES (0,0,0,0,0);
/*------------------------------------------------------------------------------*/


/*
Pareillement, cette table semble propre : pas de données nulles, toujours les
bons formats. Ceci dit, sa taille est beaucoup plus importante et je peux passer à côté
d'une info. Je serai donc particulièrement vigilante sur le nombre d'entrée de cette table,
surtout sur la date dont le format peut vite coincer.
*/
CREATE TABLE transformationDevoir.ticket(
  numTicket varchar,
  dateTicket date,
  numDefaut integer,
  numUsine integer
);

INSERT INTO transformationDevoir.ticket
	SELECT CAST(numTicket AS varchar),
         TO_DATE(dateTicket, 'YYYY-MM-DD'),
         CAST(numDefaut AS INTEGER),
         CAST(numUsine AS INTEGER) FROM extractionDevoir.ticket;

UPDATE transformationDevoir.ticket as ti
SET numDefaut = 0 WHERE ti.numDefaut not in (SELECT numDefaut FROM transformationDevoir.defautType);

UPDATE transformationDevoir.ticket as ti
SET numUsine = 0 WHERE ti.numDefaut not in (SELECT numUsine FROM transformationDevoir.usine);

/*Je ne peux pas faire l'update de la date ici car je ne peux pas ajouter la date d'erreur dans la vue représentant
les dates. Je le fais donc après la création de la vue des dates. */

/*==============================================================================*/
/*==============================================================================*/


/*
Les données sont propres et dans les bons formats, je cree les vues qui m'intéressent :
j'ajouterai une entree dans chaque vue de dimension une entrée pour une valeur de clé inconnue
puisque j'utilise des clés étrangères non artificielles.
Pour créer la vue sur les dates, j'ai besoin d'une fonction outil : ce sera getDatePart.
*/

CREATE OR REPLACE FUNCTION getDatePart(dateOriginal IN date, datePartNeeded in text) RETURNS integer AS
$$
BEGIN
RETURN CASE  WHEN datePartNeeded='day' THEN extract(day from dateOriginal)
                    WHEN datePartNeeded='month' THEN extract(month from dateOriginal)
                    WHEN datePartNeeded='isoyear' THEN extract(isoyear from dateOriginal)
                    WHEN datePartNeeded='quarter' THEN extract(quarter from dateOriginal)
                    WHEN datePartNeeded='week' THEN extract(week from dateOriginal)
                    WHEN datePartNeeded='isodow' THEN extract(isodow from dateOriginal)
                    ELSE 0 END;
END;
$$ LANGUAGE plpgsql;

CREATE TABLE transformationDevoir.dateDimensionTemp AS
  SELECT DISTINCT dateTicket,
                  getDatePart(dateTicket, 'day') AS jour,
                  getDatePart(dateTicket, 'month') AS mois,
                  getDatePart(dateTicket, 'isoyear') AS annee,
                  getDatePart(dateTicket, 'quarter') AS trimestre,
                  getDatePart(dateTicket, 'week') AS semaine,
                  getDatePart(dateTicket, 'isodow') AS jourSemaine
                FROM transformationDevoir.ticket;

CREATE OR REPLACE VIEW transformationDevoir.dateDimension AS SELECT * FROM transformationDevoir.dateDimensionTemp;

INSERT INTO transformationDevoir.dateDimension VALUES (TO_DATE('0000-00-00', 'YYYY-MM-DD'), NULL, NULL, NULL, NULL, NULL, NULL);

UPDATE transformationDevoir.ticket as ti
SET dateTicket = TO_DATE('0000-00-00', 'YYYY-MM-DD') WHERE ti.numDefaut not in (SELECT numUsine FROM transformationDevoir.usine);

CREATE OR REPLACE VIEW transformationDevoir.emplacementDimension AS
  SELECT DISTINCT numUsine,
                  usine.numDepartement,
                  nomDepartement,
                  populationDepartement,
                  anneeCreation,
                  ageUsine,
                  capaciteProduction
                FROM transformationDevoir.usine INNER JOIN transformationDevoir.departement
                ON usine.numDepartement = departement.numDepartement;


CREATE OR REPLACE VIEW transformationDevoir.typeDefautDimension AS
  SELECT DISTINCT numDefaut AS numTypeDefaut,
                  defautType.categorieDefautCode AS categorieDefautCode,
                  categorieDefautLong AS categorieDefautLong
    FROM transformationDevoir.defautType INNER JOIN transformationDevoir.categorie
    ON defautType.categorieDefautCode = categorie.categorieDefautCode;

 CREATE OR REPLACE VIEW transformationDevoir.ticketDefautFait AS
  SELECT numTicket,
  dateTicket,
  numDefaut AS numTypeDefaut,
  numUsine from transformationDevoir.ticket;
