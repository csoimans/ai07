/*Dimension Type Defaut*/

SELECT COUNT(*) FROM transformationDevoir.typeDefautDimension;
SELECT COUNT(*) FROM transformationDevoir.typeDefautDimension GROUP BY categorieDefautCode;
SELECT * FROM transformationDevoir.typeDefautDimension WHERE numTypeDefaut='1';
SELECT * FROM transformationDevoir.typeDefautDimension WHERE numTypeDefaut='44';
SELECT * FROM transformationDevoir.typeDefautDimension WHERE numTypeDefaut='21';
SELECT * FROM transformationDevoir.typeDefautDimension WHERE categorieDefautLong='Painting';


/*Dimension Emplacement*/
SELECT COUNT(*) FROM transformationDevoir.emplacementDimension;
SELECT numUsine,nomDepartement FROM transformationDevoir.emplacementDimension WHERE numDepartement='02';
SELECT numUsine,nomDepartement FROM transformationDevoir.emplacementDimension WHERE numDepartement='46';
SELECT numUsine,numDepartement FROM transformationDevoir.emplacementDimension WHERE nomDepartement='Loire';
SELECT numUsine,numDepartement FROM transformationDevoir.emplacementDimension WHERE nomDepartement='Vosges';
SELECT numUsine,nomDepartement FROM transformationDevoir.emplacementDimension WHERE populationDepartement='638198';
SELECT numUsine,nomDepartement FROM transformationDevoir.emplacementDimension WHERE populationDepartement='1165231';
SELECT numUsine,nomDepartement FROM transformationDevoir.emplacementDimension WHERE numUsine='40';
SELECT numUsine,nomDepartement FROM transformationDevoir.emplacementDimension WHERE numUsine='7';
SELECT numUsine,nomDepartement FROM transformationDevoir.emplacementDimension WHERE anneeCreation='1983';
SELECT numUsine,nomDepartement FROM transformationDevoir.emplacementDimension WHERE anneeCreation='2002';
SELECT numUsine,nomDepartement FROM transformationDevoir.emplacementDimension WHERE capaciteProduction='7';
SELECT numUsine,nomDepartement FROM transformationDevoir.emplacementDimension WHERE capaciteProduction='4';
SELECT numUsine,nomDepartement FROM transformationDevoir.emplacementDimension WHERE ageUsine='10';
SELECT numUsine,nomDepartement FROM transformationDevoir.emplacementDimension WHERE ageUsine='47';

/*Faits */
SELECT COUNT(*) FROM transformationDevoir.ticketDefautFait;
SELECT * FROM transformationDevoir.ticketDefautFait WHERE numTicket = '320040033';
