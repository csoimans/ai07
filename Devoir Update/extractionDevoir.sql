create schema extractionDevoir;

create table extractionDevoir.categorie(
  categorieDefautCode text,
  categorieDefautLong text
);

create table extractionDevoir.defautType(
  numDefaut text,
  categorieDefautCode text
);

create table extractionDevoir.departement(
  numDepartement text,
  nomDepartement text,
  populationDepartement text
);

create table extractionDevoir.usine(
  numUsine text,
  anneeCreation text,
  ageUsine text,
  capaciteProduction text,
  numDepartement text
);


create table extractionDevoir.ticket(
  numTicket text,
  dateTicket text,
  numDefaut text,
  numUsine text
);

\copy extractionDevoir.categorie FROM '/Users/cynthiasoimansoib/Downloads/ai07-master/Devoir/Category' DELIMITERS ';' CSV;
\copy extractionDevoir.defautType FROM '/Users/cynthiasoimansoib/Downloads/ai07-master/Devoir/DefaultType' DELIMITERS ';' CSV;
\copy extractionDevoir.ticket FROM '/Users/cynthiasoimansoib/Downloads/ai07-master/Devoir/Lawnmower00001' DELIMITERS ';' CSV;
\copy extractionDevoir.departement FROM '/Users/cynthiasoimansoib/Downloads/ai07-master/Devoir/dpt_fr' DELIMITERS ';' CSV;
\copy extractionDevoir.usine FROM '/Users/cynthiasoimansoib/Downloads/ai07-master/Devoir/Factory' DELIMITERS ';' CSV;

