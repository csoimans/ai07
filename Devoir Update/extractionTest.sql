/*
Tester les categories est facile, il n'y en a que 6; On regarde directement sur
la table si on a bien 6 entrées et une pour chaque lettre de A à F.
*/
SELECT COUNT(*) FROM extractionDevoir.categorie; /* on doit avoir 6*/
SELECT * FROM extractionDevoir.categorie WHERE categorieDefautCode='A'; /* Une entree pour chaque et la bonne valeur associee*/
SELECT * FROM extractionDevoir.categorie WHERE categorieDefautCode='B';
SELECT * FROM extractionDevoir.categorie WHERE categorieDefautCode='C';
SELECT * FROM extractionDevoir.categorie WHERE categorieDefautCode='D';
SELECT * FROM extractionDevoir.categorie WHERE categorieDefautCode='E';
SELECT * FROM extractionDevoir.categorie WHERE categorieDefautCode='F';

/*
Pour les types de defauts on compte le nombre d'entrees (45) et le nombre d'entrées
par categories (d'après le fichier, 6 pour A,  7 pour B et E, 11 pour C, 5 pour F, 9 pour D).
 On peut choisir deux ou trois valeurs de numDefaut au hasard et vérifier la ligne.
*/

SELECT COUNT(*) FROM extractionDevoir.defautType;
SELECT COUNT(*) FROM extractionDevoir.defautType GROUP BY categorieDefautCode;
SELECT * FROM extractionDevoir.defautType WHERE numDefaut='1';
SELECT * FROM extractionDevoir.defautType WHERE numDefaut='44';
SELECT * FROM extractionDevoir.defautType WHERE numDefaut='21';

/*
Pour les faits, on verifie le nombre d'entrées (5320 attendues) et deux trois valeurs de numTicket,
voire le nombre d'occurence d'un ticket.
*/
SELECT COUNT(*) FROM extractionDevoir.ticket;
SELECT * FROM extractionDevoir.ticket WHERE numTicket = '320040033'; /* 2 tickets aux valeurs identiques attendus*/

/*
Si on s'intéresse au département, on sait qu'il y a 95 départements dans le fichier ,on vérifie une ou deux entrées par
le nom et une ou deux entrées par la valeur exacte de population.
*/

SELECT COUNT(*) FROM extractionDevoir.departement;
SELECT * FROM extractionDevoir.departement WHERE numDepartement='02';
SELECT * FROM extractionDevoir.departement WHERE numDepartement='46';
SELECT * FROM extractionDevoir.departement WHERE nomDepartement='Paris';
SELECT * FROM extractionDevoir.departement WHERE nomDepartement='Oise';
SELECT * FROM extractionDevoir.departement WHERE populationDepartement='638198';
SELECT * FROM extractionDevoir.departement WHERE populationDepartement='1165231';

/*
Si on s'intéresse enfin aux usines, on peut tester : 
- Le nombre de lignes : il y a 52 usines enregistrées normalement
- Les années de fondation : on séléctionne une ou deux entrées par leur année de fondation
- L'âge des usines : on séléctionne une ou deux entrées par leur âge
- Pareil pour la capacité de production
- Pareil pour le type de défaut
*/

SELECT COUNT(*) FROM extractionDevoir.usine;
SELECT * FROM extractionDevoir.usine WHERE numUsine='40';
SELECT * FROM extractionDevoir.usine WHERE numUsine='7';
SELECT * FROM extractionDevoir.usine WHERE anneeCreation='1983';
SELECT * FROM extractionDevoir.usine WHERE anneeCreation='2002';
SELECT * FROM extractionDevoir.usine WHERE capaciteProduction='7';
SELECT * FROM extractionDevoir.usine WHERE capaciteProduction='4';
SELECT * FROM extractionDevoir.usine WHERE numDepartement='02';
SELECT * FROM extractionDevoir.usine WHERE numDepartement='88';
SELECT * FROM extractionDevoir.usine WHERE ageUsine='10';
SELECT * FROM extractionDevoir.usine WHERE ageUsine='47';
