create schema extractionDevoir;

create table extractionDevoir.categorie(
  categorieDefautCode text,
  categorieDefautLong text
);

create table extractionDevoir.defautType(
  numDefaut text,
  categorieDefautCode text
);
/*
create table extractionDevoir.departement(
  numDepartement text,
  nomDepartement text,
  populationDepartement text
);

create table extractionDevoir.usine(
  numUsine text,
  anneeCreation text,
  ageUsine text,
  capaciteProduction text,
  numDepartement text
);
*/

create table extractionDevoir.ticket(
  numTicket text,
  dateTicket text,
  numDefaut text,
  numUsine text
);

\copy extractionDevoir.categorie FROM '/volsme/users/bdd1p003/Bureau/Devoir/Category' DELIMITERS ';' CSV;
\copy extractionDevoir.defautType FROM '/volsme/users/bdd1p003/Bureau/Devoir/DefaultType' DELIMITERS ';' CSV;
\copy extractionDevoir.ticket FROM '/volsme/users/bdd1p003/Bureau/Devoir/Lawnmower00001' DELIMITERS ';' CSV;

/*
\copy extractionDevoir.departement FROM '/volsme/users/bdd1p003/Bureau/Devoir/DefaultType' DELIMITERS ',' CSV;
\copy extractionDevoir.usine FROM '/Users/cynthiasoimansoib/Desktop/AI07/csvFiles/departementsInsee2003.txt' DELIMITERS ';' CSV;
*/
