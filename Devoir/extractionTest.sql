/*
Tester les categories est facile, il n'y en a que 6; On regarde directement sur
la table si on a bien 6 entrées et une pour chaque lettre de A à F.
*/
SELECT COUNT(*) FROM extractionDevoir.categorie; /* on doit avoir 6*/
SELECT * FROM extractionDevoir.categorie WHERE categorieDefautCode='A'; /* Une entree pour chaque et la bonne valeur associee*/
SELECT * FROM extractionDevoir.categorie WHERE categorieDefautCode='B';
SELECT * FROM extractionDevoir.categorie WHERE categorieDefautCode='C';
SELECT * FROM extractionDevoir.categorie WHERE categorieDefautCode='D';
SELECT * FROM extractionDevoir.categorie WHERE categorieDefautCode='E';
SELECT * FROM extractionDevoir.categorie WHERE categorieDefautCode='F';

/*
Pour les types de defauts on compte le nombre d'entrees (45) et le nombre d'entrées
par categories (d'après le fichier, 6 pour A,  7 pour B et E, 11 pour C, 5 pour F, 9 pour D).
 On peut choisir deux ou trois valeurs de numDefaut au hasard et vérifier la ligne.
*/

SELECT COUNT(*) FROM extractionDevoir.defautType;
SELECT COUNT(*) FROM extractionDevoir.defautType GROUP BY categorieDefautCode;
SELECT * FROM extractionDevoir.defautType WHERE numDefaut='1';
SELECT * FROM extractionDevoir.defautType WHERE numDefaut='44';
SELECT * FROM extractionDevoir.defautType WHERE numDefaut='21';

/*
Pour les faits, on verifie le nombre d'entrées (5320 attendues) et deux trois valeurs de numTicket,
voire le nombre d'occurence d'un ticket.
*/
SELECT COUNT(*) FROM extractionDevoir.ticket;
SELECT * FROM extractionDevoir.ticket WHERE numTicket = '320040033'; /* 2 tickets aux valeurs identiques attendus*/
