CREATE SCHEMA transformationDevoir;


/*
La table des categories est très propre : tout est renseigné et déjà au bon format.
Je change le type dès maintenant.
*/
CREATE TABLE transformationDevoir.categorie(
  categorieDefautCode char(1),
  categorieDefautLong varchar
);

INSERT INTO transformationDevoir.categorie
	SELECT CAST(categorieDefautCode AS CHAR),CAST(categorieDefautLong AS VARCHAR)
  FROM extractionDevoir.categorie;

  /*
  Pareillement, cette table semble propre : pas de données nulles, toujours les bons formats.
  Je change le type dès maintenant.
  */
CREATE TABLE transformationDevoir.defautType(
  numDefaut integer,
  categorieDefautCode char(1)
);

INSERT INTO transformationDevoir.defautType
	SELECT CAST(numDefaut AS INTEGER),CAST(categorieDefautCode AS CHAR)
  FROM extractionDevoir.defautType;


  /*
  Pareillement, cette table semble propre : pas de données nulles, toujours les bons formats.
  Ceci dit, sa taille est beaucoup plus importante et je peux passer à côté d'une info.
  Je serai donc particulièrement vigilante sur le nombre d'entrée de cette table, surtout sur la date.
  */
CREATE TABLE transformationDevoir.ticket(
  numTicket varchar,
  dateTicket date,
  numDefaut integer,
  numUsine integer
);

INSERT INTO transformationDevoir.ticket
	SELECT CAST(numTicket AS varchar),TO_DATE(dateTicket, 'YYYY-MM-DD'), CAST(numDefaut AS INTEGER),
  CAST(numUsine AS INTEGER) FROM extractionDevoir.ticket;

/* L'hypothese de proprete sur cette table se verifie si je la teste à l'oeil nu donc je poursuis*/

/* Les données sont propres et dans les bons formats, je cree les vues qui m'intéressent : j'ajouterai
une entree dans chaque vue de dimension une entrée pour une valeur de clé inconnue */
CREATE OR REPLACE VIEW transformationDevoir.typeDefautDimension AS
SELECT DISTINCT numDefaut as numTypeDefaut, defautType.categorieDefautCode as categorieDefautCode, categorieDefautLong as categorieDefautLong
from transformationDevoir.defautType
 INNER JOIN transformationDevoir.categorie ON defautType.categorieDefautCode = categorie.categorieDefautCode;

 CREATE OR REPLACE VIEW transformationDevoir.ticketDefautFait AS
 SELECT * from transformationDevoir.ticket;

 /* A ce stade, je pourrai crééer directement la zone de load et l'interroger mais je manque de temps alors je vais poser quelques questions directement sur mes vues
 Il manque les contraintes de clés (mais le select distinct a normalement géré l'unicité). Tant pis.

Si je veux savoir quelle est la proportion de défaut selon la catégorie.
Il faudrait mettre ça en proportion (je n'ai plus le temps)
 */
SELECT categorieDefautCode, categorieDefautLong, count(*) FROM transformationDevoir.typeDefautDimension INNER JOIN transformationDevoir.ticketDefautFait
ON ticketDefautFait.numDefaut = typeDefautDimension.numTypeDefaut GROUP BY categorieDefautCode, categorieDefautLong;
