/* Defauts en fonction de l'usine et de sa capacite de production */
\copy (SELECT emplacement.numUsine, COUNT(*) as nbDefaut, ROUND((CAST(COUNT(*) as NUMERIC )/ emplacement.capaciteProduction ),2) as proportionDefautUsine FROM loadDevoir.ticketDefaut INNER JOIN loadDevoir.emplacement ON ticketDefaut.numUsine = emplacement.numUsine GROUP BY emplacement.numUsine ORDER BY proportionDefautUsine DESC) to 'defautUsine.csv' csv header;

SELECT emplacement.numUsine, COUNT(*) as nbDefaut, ROUND((CAST(COUNT(*) as NUMERIC )
/ emplacement.capaciteProduction ),2) as proportionDefautUsine
 FROM loadDevoir.ticketDefaut INNER JOIN
loadDevoir.emplacement ON ticketDefaut.numUsine = emplacement.numUsine
GROUP BY emplacement.numUsine ORDER BY proportionDefautUsine DESC;


/* Categorie de defauts dans le cas des defauts multiples*/
\copy (WITH multipleDefaults AS (SELECT numTicket, COUNT(*) from loadDevoir.ticketDefaut GROUP BY numTicket HAVING  COUNT(*) > 1) SELECT count(*),fait.numTicket, categorieDefautCode from multipleDefaults INNER JOIN  (loadDevoir.ticketDefaut a INNER JOIN loadDevoir.typeDefaut b ON a.numTypeDefaut = b.numTypeDefaut) fait ON multipleDefaults.numTicket = fait.numTicket GROUP BY fait.numTicket;) to 'defaut.csv' csv header;

WITH multipleDefaults AS (SELECT numTicket, COUNT(*) from loadDevoir.ticketDefaut GROUP BY numTicket HAVING  COUNT(*) > 1)
SELECT count(*),fait.numTicket, categorieDefautCode from multipleDefaults INNER JOIN  (loadDevoir.ticketDefaut a INNER JOIN loadDevoir.typeDefaut b
 ON a.numTypeDefaut = b.numTypeDefaut) fait
ON multipleDefaults.numTicket = fait.numTicket GROUP BY fait.numTicket;


/* Defauts en fonction de la date : date complete */
 
WITH totalTable AS (SELECT COUNT(*) as totalDefauts FROM loadDevoir.ticketDefaut)
SELECT dateTicket.dateDefaut, COUNT(*) as nbDefaut, ROUND((CAST(COUNT(*) as NUMERIC )
/ totalTable.totalDefauts)*100,2) as proportionDefautTotal
 FROM totalTable,loadDevoir.ticketDefaut INNER JOIN
loadDevoir.dateTicket ON ticketDefaut.dateTicket = dateTicket.dateDefaut
GROUP BY dateTicket.dateDefaut, totalTable.totalDefauts ORDER BY proportionDefautTotal DESC;


/* Defauts en fonction de la date : mois */

\copy (WITH totalTable AS (SELECT COUNT(*) as totalDefauts FROM loadDevoir.ticketDefaut) SELECT dateTicket.mois, COUNT(*) as nbDefaut, ROUND((CAST(COUNT(*) as NUMERIC )/ totalTable.totalDefauts)*100,2) as proportionDefautTotal FROM totalTable,loadDevoir.ticketDefaut INNER JOIN loadDevoir.dateTicket ON ticketDefaut.dateTicket = dateTicket.dateDefaut GROUP BY dateTicket.mois, totalTable.totalDefauts ORDER BY proportionDefautTotal DESC) to 'defautDateMois.csv' csv header;
WITH totalTable AS (SELECT COUNT(*) as totalDefauts FROM loadDevoir.ticketDefaut)
SELECT dateTicket.mois, COUNT(*) as nbDefaut, ROUND((CAST(COUNT(*) as NUMERIC )
/ totalTable.totalDefauts)*100,2) as proportionDefautTotal
 FROM totalTable,loadDevoir.ticketDefaut INNER JOIN
loadDevoir.dateTicket ON ticketDefaut.dateTicket = dateTicket.dateDefaut
GROUP BY dateTicket.mois, totalTable.totalDefauts ORDER BY proportionDefautTotal DESC;


/* Defauts en fonction de la date : jour de la semaine */

WITH totalTable AS (SELECT COUNT(*) as totalDefauts FROM loadDevoir.ticketDefaut)
SELECT dateTicket.jourSemaine, COUNT(*) as nbDefaut, ROUND((CAST(COUNT(*) as NUMERIC )
/ totalTable.totalDefauts)*100,2) as proportionDefautTotal
 FROM totalTable,loadDevoir.ticketDefaut INNER JOIN
loadDevoir.dateTicket ON ticketDefaut.dateTicket = dateTicket.dateDefaut
GROUP BY dateTicket.jourSemaine, totalTable.totalDefauts ORDER BY proportionDefautTotal DESC;


/* Defauts en fonction de la date : jour de la semaine d'un mois avec beaucoup de defauts */

WITH totalTable AS (SELECT COUNT(*) as totalDefauts FROM loadDevoir.ticketDefaut a INNER JOIN loadDevoir.dateTicket b ON 
a.dateTicket = b.dateDefaut WHERE mois=1 OR mois=4 OR mois=5)
SELECT dateTicket.jour, COUNT(*) as nbDefaut, ROUND((CAST(COUNT(*) as NUMERIC )
/ totalTable.totalDefauts)*100,2) as proportionDefautTotal
 FROM totalTable, loadDevoir.ticketDefaut INNER JOIN loadDevoir.dateTicket ON 
ticketDefaut.dateTicket = dateTicket.dateDefaut WHERE mois=1 OR mois=4 OR mois=5 
GROUP BY dateTicket.jour, totalTable.totalDefauts ORDER BY proportionDefautTotal DESC;

/* Defauts en fonction de la categorie de defaut */
\copy (WITH totalTable AS (SELECT COUNT(*) as totalDefauts FROM loadDevoir.ticketDefaut)SELECT categorieDefautCode, categorieDefautLong, COUNT(*) as nbDefaut,ROUND((CAST(COUNT(*) as NUMERIC )/ totalTable.totalDefauts)*100,2) as proportionDefaut FROM totalTable, loadDevoir.ticketDefaut INNER JOIN loadDevoir.typeDefaut ON ticketDefaut.numTypeDefaut = typeDefaut.numTypeDefaut GROUP BY categorieDefautCode, categorieDefautLong, totalTable.totalDefauts ORDER BY nbDefaut DESC) to 'defautCategorie.csv' csv header;

WITH totalTable AS (SELECT COUNT(*) as totalDefauts FROM loadDevoir.ticketDefaut)
SELECT categorieDefautCode, categorieDefautLong, COUNT(*) as nbDefaut,ROUND((CAST(COUNT(*) as NUMERIC )
/ totalTable.totalDefauts)*100,2) as proportionDefaut FROM totalTable, loadDevoir.ticketDefaut 
INNER JOIN loadDevoir.typeDefaut ON ticketDefaut.numTypeDefaut = typeDefaut.numTypeDefaut 
GROUP BY categorieDefautCode, categorieDefautLong, totalTable.totalDefauts ORDER BY nbDefaut DESC;

