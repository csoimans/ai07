/* Question 1 : Analyser les faits en fonction des departements de fabrication */

/* Ici il sera interessant de mettre en proportion avec le nombre d'usines dans un departement.
Si un departement contient 3 fois plus d'usine que les autres et que son nombre de defauts releves est
plus important, on ne doit pas passer a cote de cette information*/

WITH totalTable AS (SELECT COUNT(*) as totalDefauts FROM loadDevoir.ticketDefaut),
nbUsineParDepartement AS (SELECT numDepartement, COUNT(numUsine) as nbUsines FROM loadDevoir.emplacement GROUP BY numDepartement)
SELECT a.numDepartement, b.nbUsines,  COUNT(*) as nbDefaut, ROUND((CAST(COUNT(*) as NUMERIC )
/ totalTable.totalDefauts)*100,2) as proportionTotalDefaut, ROUND((CAST(COUNT(*) as NUMERIC )
/ (totalTable.totalDefauts * b.nbUsines) )*100,2)as proportionUsineDepartement
 FROM totalTable,(loadDevoir.ticketDefaut INNER JOIN
loadDevoir.emplacement ON ticketDefaut.numUsine = emplacement.numUsine) a INNER JOIN nbUsineParDepartement b ON
a.numDepartement = b.numDepartement
GROUP BY totalTable.totalDefauts,a.numDepartement,b.nbUsines ORDER BY proportionUsineDepartement DESC;

/* Le departement 88 qui contient 6 usines a le plus gros pourcentage de defaut global et conserve la proportion la plus haute
par usine d'erreur (meme apres repartition du total d'erreur sur les 6 usines). Le departement 64 a une proportion presque aussi grande d'erreur
avec une seule usine. */

/* Export CSV pour exploitation tableur */

\copy (WITH totalTable AS (SELECT COUNT(*) as totalDefauts FROM loadDevoir.ticketDefaut), nbUsineParDepartement AS (SELECT numDepartement, COUNT(numUsine) as nbUsines FROM loadDevoir.emplacement GROUP BY numDepartement) SELECT a.numDepartement, b.nbUsines,  COUNT(*) as nbDefaut, ROUND((CAST(COUNT(*) as NUMERIC )/ totalTable.totalDefauts)*100,2) as proportionTotalDefaut, ROUND((CAST(COUNT(*) as NUMERIC )/ (totalTable.totalDefauts * b.nbUsines) )*100,2)as proportionUsineDepartement FROM totalTable,(loadDevoir.ticketDefaut INNER JOIN loadDevoir.emplacement ON ticketDefaut.numUsine = emplacement.numUsine) a INNER JOIN nbUsineParDepartement b ON a.numDepartement = b.numDepartement GROUP BY totalTable.totalDefauts,a.numDepartement,b.nbUsines ORDER BY proportionUsineDepartement DESC) to 'defautParDepartement.csv' csv header;

/* Question 2 : Analyser les faits en fonction de la capacite des usines */

/* Avant d'analyser ces defauts, il faut prendre en compte le nombre d'usine qui ont cette capacite :
si il y a plus d'usines qui on une capacite de 4 elements par heure, les defauts seront naturellement plus nombreux
pour une capacite a 4 : pour le faire, je choisis de diviser le nombre d'entree par le nombre d'usine ayant cette capacite.
De cette facon le chiffre mis en pourcentage represente toujours plus ou moins une usine. En revanche ce n'est plus tout a fait
un pourcentage*/

WITH totalTable AS (SELECT COUNT(*) as totalDefauts FROM loadDevoir.ticketDefaut),
nbUsineParCapacite AS (SELECT capaciteProduction, COUNT(numUsine) as nbUsines FROM loadDevoir.emplacement GROUP BY capaciteProduction)
SELECT a.capaciteProduction, b.nbUsines,  COUNT(*) as nbDefaut, ROUND((CAST(COUNT(*) as NUMERIC )
/ totalTable.totalDefauts)*100,2) as proportionTotalDefaut, ROUND((CAST(COUNT(*) as NUMERIC )
/ (totalTable.totalDefauts * b.nbUsines) )*100,2)as proportionUneUsine
 FROM totalTable,(loadDevoir.ticketDefaut INNER JOIN
loadDevoir.emplacement ON ticketDefaut.numUsine = emplacement.numUsine) a INNER JOIN nbUsineParCapacite b ON
a.capaciteProduction = b.capaciteProduction
GROUP BY totalTable.totalDefauts,a.capaciteProduction,b.nbUsines ORDER BY proportionUneUsine DESC;


/* Sans mettre en proportion le resultat on constate que seulement 3.43 % des defauts sont lies a une capacite de 10, ce qui est environ
5 fois moins qu'une capacite a 4. En revanche si on s'interesse au pourcentage de defaut que ca representerait pour une usine, on a le chiffre
le plus eleve pour une usine avec la capacite de 10. Une seule usine a 10 capacite genere 2 fois plus de defaut qu'une seule usine avec une capacite de 4
(puisque avec ma methode je repartis (artificiellement) les defauts sur le nombre d'usine avec la capacite)*/

/* Export CSV pour exploitation tableur */

\copy (WITH totalTable AS (SELECT COUNT(*) as totalDefauts FROM loadDevoir.ticketDefaut), nbUsineParCapacite AS (SELECT capaciteProduction, COUNT(numUsine) as nbUsines FROM loadDevoir.emplacement GROUP BY capaciteProduction) SELECT a.capaciteProduction, b.nbUsines,  COUNT(*) as nbDefaut, ROUND((CAST(COUNT(*) as NUMERIC )/ totalTable.totalDefauts)*100,2) as proportionTotalDefaut, ROUND((CAST(COUNT(*) as NUMERIC )/ (totalTable.totalDefauts * b.nbUsines) )*100,2)as proportionUneUsine FROM totalTable,(loadDevoir.ticketDefaut INNER JOIN loadDevoir.emplacement ON ticketDefaut.numUsine = emplacement.numUsine) a INNER JOIN nbUsineParCapacite b ON  a.capaciteProduction = b.capaciteProduction GROUP BY totalTable.totalDefauts,a.capaciteProduction,b.nbUsines ORDER BY proportionUneUsine DESC) to 'defautParCapacite.csv' csv header;

/* Question 3 : Analyser les faits en fonction des categories de defaut
Question en français : quelle est l'influence de la categorie de defaut sur le nombre de defauts reportes */

/* Ici on aurait pu mettre en proportion par le nombre de defauts que contient chaque categorie mais d'un point de vue
logique, ca ne me parait pas avoir de sens. Chaque numero de defaut correspond a un defaut particulier et je ne pense pas que
le fait qu'il y en ait plus dans une categorie indique quoi que ce soit par rapport a la frequence du defaut. Il s'agit simplement
de les classifier. */


WITH totalTable AS (SELECT COUNT(*) as totalDefauts FROM loadDevoir.ticketDefaut)
SELECT categorieDefautCode, categorieDefautLong, COUNT(*) as nbDefaut,ROUND((CAST(COUNT(*) as NUMERIC )
/ totalTable.totalDefauts)*100,2) as proportionDefaut FROM totalTable, loadDevoir.ticketDefaut
INNER JOIN loadDevoir.typeDefaut ON ticketDefaut.numTypeDefaut = typeDefaut.numTypeDefaut
GROUP BY categorieDefautCode, categorieDefautLong, totalTable.totalDefauts ORDER BY nbDefaut DESC;


/* On a clairement une difference significative dans la repartition pour la categorie A : elle regroupe 2
fois plus de defauts environ que la deuxieme categorie avec le plus de defaut. */

/* Export CSV pour exploitation tableur */

\copy (WITH totalTable AS (SELECT COUNT(*) as totalDefauts FROM loadDevoir.ticketDefaut) SELECT categorieDefautCode, categorieDefautLong, COUNT(*) as nbDefaut,ROUND((CAST(COUNT(*) as NUMERIC )/ totalTable.totalDefauts)*100,2) as proportionDefaut FROM totalTable, loadDevoir.ticketDefaut INNER JOIN loadDevoir.typeDefaut ON ticketDefaut.numTypeDefaut = typeDefaut.numTypeDefaut GROUP BY categorieDefautCode, categorieDefautLong, totalTable.totalDefauts ORDER BY nbDefaut DESC) to 'defautCategorieDefaut.csv' csv header;


/* Question 4 : Etudier les faits en fonction du mois de l'annee.
Question en français : quelle est l'influence du mois de l'annee sur le nombre de defauts reportes */

/* Requete SQL avec proportion */

WITH totalTable AS (SELECT COUNT(*) as totalDefauts FROM loadDevoir.ticketDefaut)
SELECT dateTicket.mois, COUNT(*) as nbDefaut, ROUND((CAST(COUNT(*) as NUMERIC )
/ totalTable.totalDefauts)*100,2) as proportionDefautTotal
 FROM totalTable,loadDevoir.ticketDefaut INNER JOIN
loadDevoir.dateTicket ON ticketDefaut.dateTicket = dateTicket.dateDefaut
GROUP BY dateTicket.mois, totalTable.totalDefauts ORDER BY proportionDefautTotal DESC;

/* Si le mois n'avait aucune influence, on aurait logiquement une repartition normale du nombre de defauts :
environ totalDefaut/12, avec une marge d'erreur. Or on constate que la plupart des mois sont en dessous de 10% et
que 3 d'entre eux excedent 15%. On a donc une différence significative avec ce qu'on pourrait attendre */

/* Export CSV pour exploitation tableur */

\copy (WITH totalTable AS (SELECT COUNT(*) as totalDefauts FROM loadDevoir.ticketDefaut) SELECT dateTicket.mois, COUNT(*) as nbDefaut, ROUND((CAST(COUNT(*) as NUMERIC )/ totalTable.totalDefauts)*100,2) as proportionDefautTotal FROM totalTable,loadDevoir.ticketDefaut INNER JOIN loadDevoir.dateTicket ON ticketDefaut.dateTicket = dateTicket.dateDefaut GROUP BY dateTicket.mois, totalTable.totalDefauts ORDER BY proportionDefautTotal DESC) to 'defautDateMois.csv' csv header;

/* A partir des csv obtenus, on peut creer des graphiques afin de representer nos resultats (c'est ce que j'ai fait pour la question libre,
je ne le ferai pas ici car je ne connais pas bien libre office)*/

/*
Conclusion sur nos resultats :

Les differents axes d'analyse ont fait ressortir les points suivants :
Dans l'annee, les mois de janvier, avril et mai sont ceux durant lesquels le plus de defauts sont remontes.
L'usine qui genere le plus de defaut a elle seule est l'usine de capacite 10 (elle est seule, il nous est facile de la retrouver et de l'identifier)
Cette usine porte le numero 31 et se situe dans les Pyrennes Atlantiques (departement 64). Il aurait ete interessant d'avoir des informations sur les references de tondeuses que produit
cette usine en particulier pour voir si elles sont radicalement differentes des autres usines ou similaires.
Le resultat par departement confirme celui par capacite : le departement 64 est la deuxieme en proportion de nombre de defaut et ne contient bien qu'une seule usine.
Le departement dans lequel on trouve le plus de defauts relates est le 88 (les Vosges). Il contient 6 usines mais meme en repartissant les defauts sur son nombre d'usine,
chaque usine a un "taux" de defaut plus eleve que dans les autres departements.
Quand a la categorie, les defauts de peinture sont au moins 2 fois plus nombreux que les autres categories (plus pour les categories autres que 'Body').

On se garde d'interpreter ces resultats, n'ayant pas connaissances de details particuliers de chaque categorie (on pourrait tres bien par exemple 
avoir un pic d'activite en mai avant que tout le monde soit en vacances ou une remontee automatique d'un des fichiers de defaut fixee au mois de janvier ...).
*/
