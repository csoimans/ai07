SELECT COUNT(*) FROM loadDevoir.typeDefaut;
SELECT COUNT(*) FROM loadDevoir.typeDefaut GROUP BY categorieDefautCode;
SELECT * FROM loadDevoir.typeDefaut WHERE numTypeDefaut='1';
SELECT * FROM loadDevoir.typeDefaut WHERE numTypeDefaut='44';
SELECT * FROM loadDevoir.typeDefaut WHERE numTypeDefaut='21';
SELECT * FROM loadDevoir.typeDefaut WHERE categorieDefautLong='Painting';


/*Dimension Emplacement*/
SELECT COUNT(*) FROM loadDevoir.emplacement;
SELECT numUsine,nomDepartement FROM loadDevoir.emplacement WHERE numDepartement='02';
SELECT numUsine,nomDepartement FROM loadDevoir.emplacement WHERE numDepartement='46';
SELECT numUsine,numDepartement FROM loadDevoir.emplacement WHERE nomDepartement='Loire';
SELECT numUsine,numDepartement FROM loadDevoir.emplacement WHERE nomDepartement='Vosges';
SELECT numUsine,nomDepartement FROM loadDevoir.emplacement WHERE populationDepartement='638198';
SELECT numUsine,nomDepartement FROM loadDevoir.emplacement WHERE populationDepartement='1165231';
SELECT numUsine,nomDepartement FROM loadDevoir.emplacement WHERE numUsine='40';
SELECT numUsine,nomDepartement FROM loadDevoir.emplacement WHERE numUsine='7';
SELECT numUsine,nomDepartement FROM loadDevoir.emplacement WHERE anneeCreation='1983';
SELECT numUsine,nomDepartement FROM loadDevoir.emplacement WHERE anneeCreation='2002';
SELECT numUsine,nomDepartement FROM loadDevoir.emplacement WHERE capaciteProduction='7';
SELECT numUsine,nomDepartement FROM loadDevoir.emplacement WHERE capaciteProduction='4';
SELECT numUsine,nomDepartement FROM loadDevoir.emplacement WHERE ageUsine='10';
SELECT numUsine,nomDepartement FROM loadDevoir.emplacement WHERE ageUsine='47';

/*Faits */
SELECT COUNT(*) FROM loadDevoir.ticketDefaut;
SELECT * FROM loadDevoir.ticketDefaut WHERE numTicket = '320040033';
