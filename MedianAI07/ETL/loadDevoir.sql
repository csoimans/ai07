CREATE SCHEMA loadDevoir;

CREATE TABLE loadDevoir.emplacement(
	numUsine INTEGER PRIMARY KEY,
	numDepartement INTEGER,
	nomDepartement VARCHAR,
	populationDepartement INTEGER,
	tranchePopulationDepartement INTEGER,
	anneeCreation INTEGER,
	ageUsine INTEGER,
	trancheAgeUsine INTEGER,
	capaciteProduction INTEGER,
	CONSTRAINT c_trancheAgeUsine CHECK (trancheAgeUsine BETWEEN 1 AND 5),
	CONSTRAINT c_tranchePopulationDepartement CHECK (tranchePopulationDepartement BETWEEN 1 AND 10)
);


/*
J'ai géré tous les cas d'erreurs en amont dans la zone T avec une entrée pour chaque valeur clé potentiellement absente des
références.
Toutes les données sont propres, en dehors de ce seul problème, pas de soucis particulier.
*/
CREATE OR REPLACE FUNCTION insertEmplacementInfo() RETURNS void
AS $$
BEGIN

    INSERT INTO loadDevoir.emplacement SELECT
    			numUsine,
                numDepartement,
                nomDepartement,
                populationDepartement,
                (CASE WHEN populationDepartement<125000 THEN 1
                    WHEN populationDepartement BETWEEN 125000 AND 250000 THEN 2
                    WHEN populationDepartement BETWEEN 250000 AND 500000 THEN 3
                    WHEN populationDepartement BETWEEN 500000 AND 750000 THEN 4
                    WHEN populationDepartement BETWEEN 750000 AND 1000000 THEN 5
                    WHEN populationDepartement BETWEEN 1000000 AND 1125000 THEN 6
                    WHEN populationDepartement BETWEEN 1125000 AND 1250000 THEN 7
                    WHEN populationDepartement BETWEEN 1250000 AND 1500000 THEN 8
                    WHEN populationDepartement >1500000 THEN 9 END) as tranchePopulationDepartement,
                anneeCreation,
                ageUsine,
                (CASE WHEN ageUsine<=10 THEN 1
                    WHEN ageUsine<=20 THEN 2
                    WHEN ageUsine<=30 THEN 3
                    WHEN ageUsine<=40 THEN 4
                    WHEN ageUsine>40 THEN 5 END) as trancheAgeUsine,
                capaciteProduction FROM transformationDevoir.emplacementDimension;
END;
$$ LANGUAGE plpgsql;

SELECT insertEmplacementInfo();

CREATE TABLE loadDevoir.dateTicket(
	dateDefaut DATE PRIMARY KEY,
	jour INTEGER,
	mois INTEGER,
	annee INTEGER,
	trimestre INTEGER,
	semaine INTEGER,
	jourSemaine INTEGER,
	CONSTRAINT c_jour CHECK (jour BETWEEN 1 AND 366),
	CONSTRAINT c_mois CHECK (mois BETWEEN 1 AND 12),
	CONSTRAINT c_trimestre CHECK (trimestre BETWEEN 1 AND 4),
	CONSTRAINT c_semaine CHECK (semaine BETWEEN 1 AND 53),
	CONSTRAINT c_jourSemaine CHECK (jourSemaine BETWEEN 1 AND 7)
);

CREATE OR REPLACE FUNCTION insertDimensionDate() RETURNS void
AS $$
BEGIN
    INSERT INTO loadDevoir.dateTicket SELECT * FROM transformationDevoir.dateDimension;
END;
$$ LANGUAGE plpgsql;

SELECT insertDimensionDate();

CREATE TABLE loadDevoir.typeDefaut(
	numTypeDefaut INTEGER PRIMARY KEY,
	categorieDefautCode CHAR(1),
	categorieDefautLong VARCHAR,
	defautFrequent BOOLEAN,
	CONSTRAINT c_categorieDefautCode CHECK (categorieDefautCode='A' OR categorieDefautCode='B' OR categorieDefautCode='C' OR
	 categorieDefautCode='D' OR categorieDefautCode='E' OR categorieDefautCode='F' OR categorieDefautCode='U'),
	CONSTRAINT c_categorieDefautLong CHECK (categorieDefautLong='Painting' OR categorieDefautLong='Engine' OR categorieDefautLong='Body' OR
	 categorieDefautLong='Electricity' OR categorieDefautLong='Electronic' OR categorieDefautLong='Equipment' OR categorieDefautLong='Unknown')
);

CREATE OR REPLACE FUNCTION insertDimensionTypeDefaut() RETURNS void
AS $$
BEGIN
    INSERT INTO loadDevoir.typeDefaut
    SELECT 	numTypeDefaut,
    		categorieDefautCode,
    		categorieDefautLong,
    		null
    FROM transformationDevoir.typeDefautDimension;
END;
$$ LANGUAGE plpgsql;

SELECT insertDimensionTypeDefaut();

CREATE TABLE loadDevoir.ticketDefaut(
	numTicket VARCHAR,
	dateTicket DATE REFERENCES loadDevoir.dateTicket(dateDefaut),
	numTypeDefaut INTEGER REFERENCES loadDevoir.typeDefaut(numTypeDefaut),
	numUsine INTEGER REFERENCES loadDevoir.emplacement(numUsine)
);


CREATE OR REPLACE FUNCTION insertFait() RETURNS void
AS $$
BEGIN
    INSERT INTO loadDevoir.ticketDefaut SELECT * FROM transformationDevoir.ticketDefautFait;
END;
$$ LANGUAGE plpgsql;

SELECT insertFait();

CREATE OR REPLACE FUNCTION defautFrequent() RETURNS void
AS $$
BEGIN
    UPDATE loadDevoir.typeDefaut as td
 		SET defautFrequent = (CASE
 								WHEN td.numTypeDefaut in (SELECT numTypeDefaut FROM loadDevoir.ticketDefaut
 									GROUP BY numTypeDefaut ORDER BY COUNT(*) DESC LIMIT 10)
 								THEN TRUE
 								ELSE FALSE END);
END;
$$ LANGUAGE plpgsql;

SELECT defautFrequent();
