CREATE SCHEMA extraction;

CREATE TABLE extraction.caisse(
	numTicket text,
	dateVente text,
	isbn text,
	magasin text
);

CREATE TABLE extraction.catalogue(
	reference text,
	isbn text,
	titre text,
	auteurs text,
	langue text,
	datePublication text,
	editeur text,
	tags text,
	genre text
);

CREATE TABLE extraction.magasin(
	magasin text,
	numDepartement text,
	typeRayonCourt text,
	typeRayonLong text,
	rayonBestsellers text,
	rayonRecents text
);

CREATE TABLE extraction.departement(
	numDepartement text,
	nomDepartement text,
	populationDepartement text
);

/*
Commandes non SQL :
- wget 'http://pic.crzt.fr/ai07/fantastic/Fantastic';
- wget 'http://pic.crzt.fr/ai07/fantastic/departementsInsee2003.txt';
- wget 'http://pic.crzt.fr/ai07/fantastic/marketing.ods';

Récupération des données depuis la base Oracle et transformation en CSV

Conversion du document .ods en CSV
- libreoffice --headless --convert-to csv marketing.ods

*/

\copy extraction.caisse FROM '/Users/cynthiasoimansoib/Desktop/AI07/csvFiles/Fantastic' DELIMITERS ';' CSV;
\copy extraction.catalogue FROM '/Users/cynthiasoimansoib/Desktop/AI07/csvFiles/export.csv' DELIMITERS ',' CSV;
\copy extraction.magasin FROM '/Users/cynthiasoimansoib/Desktop/AI07/csvFiles/marketing.csv' DELIMITERS ',' CSV;
\copy extraction.departement FROM '/Users/cynthiasoimansoib/Desktop/AI07/csvFiles/departementsInsee2003.txt' DELIMITERS ';' CSV;
