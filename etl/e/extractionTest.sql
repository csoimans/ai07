/*
	Par lecture des données dans leur format original, nous avons les nombres de lignes de tous les fichiers.
	On commence par requêter le nombre de lignes des tables et vérifier que ça correspond.
*/

SELECT COUNT(*) FROM extraction.caisse;
SELECT COUNT(*) FROM extraction.catalogue;
SELECT COUNT(*) FROM extraction.magasin;
SELECT COUNT(*) FROM extraction.departement;

/*
	On effectue ensuite des tests sur des données précises pour vérifier qu'elles sont passées.
	Pour faciliter le test, on choisira des données facilement controlable.
	Par exemple, le catalogue contenait exactement 3 livres en allemands dont les titres me sont connus.
	On peut trouver le nombre d'occurences d'un magasin dans le fichier Fantastic original et le comparer 
	à un count de ces occurences dans la table.
	Pour les tickets de caisse, on peut rechercher un numéro de ticket connu. Pour être sûre que même 
	les données étranges comme les dates incompletes ou champs non renseignés sont bien passés, on peut 
	effectuer une sélection dessus.
	Les départements comportent peu d'entrées 
*/

SELECT titre FROM extraction.catalogue WHERE langue = 'de';
SELECT * FROM extraction.catalogue WHERE editeur = '';
SELECT * FROM extraction.catalogue WHERE isbn = '';

SELECT * FROM extraction.caisse WHERE isbn IS NULL OR isbn SIMILAR TO ' *';
SELECT * FROM extraction.caisse WHERE dateVente SIMILAR TO '-[\d]{2}-[\d]{2}';
SELECT COUNT(*) FROM extraction.caisse WHERE magasin = 'M62';

SELECT * FROM extraction.departement WHERE numDepartement = '';
SELECT nomDepartement FROM extraction.departement WHERE populationDepartement = '';

SELECT typeRayonnageCourt, COUNT(*) FROM extraction.magasin GROUP BY typeRayonnageCourt;
SELECT numDepartement FROM extraction.magasin WHERE magasin = 'M40';


