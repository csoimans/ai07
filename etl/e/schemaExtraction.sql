create schema fbde;

create table fbde.catalogue(
	ref text,
	isbn text,
	title text,
	authors text,
	language text,
	pubdate text,
	publisher text,
	tags text,
	genre text
);

create table fbde.departement(
	numeroDep text,
	nomDep text,
	population text
);

create table fbde.caisse(
	numero text,
	date text,
	isbn text,
	store text
);

create table fbde.marketing(
	magasin text,
	numDepartement text,
	rayonnageAbr text,
	rayonnageComplet text,
	bestseller text,
	recent text
);

/* Exemples simples de vérification 

select title from fbde.catalogue where language="deu"; // Tester une entrée particulière pour voir si on a le nombre de livres attendus
select count(*) from fdbe.departement; //Vérifier la taille des données

*/