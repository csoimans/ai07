CREATE SCHEMA load;

CREATE TABLE load.temps(
  dateVente date,
  jour integer,
  mois integer,
  annee integer,
  saison integer,
  semaine integer,
  jourSemaine integer,
  CONSTRAINT c_jour CHECK (jour BETWEEN 1 AND 366),
  CONSTRAINT c_mois CHECK (mois BETWEEN 1 AND 12),
  CONSTRAINT c_saison CHECK (saison BETWEEN 1 AND 4),
  CONSTRAINT c_semaine CHECK (semaine BETWEEN 1 AND 53),
  CONSTRAINT c_jourSemaine CHECK (jourSemaine BETWEEN 1 AND 7),
  CONSTRAINT c_dateVenteCle PRIMARY KEY (dateVente)
);


CREATE TABLE load.tempsError(
  dateVente date,
  jour integer,
  mois integer,
  annee integer,
  saison integer,
  semaine integer,
  jourSemaine integer
);

CREATE OR REPLACE FUNCTION insertDimensionDate() RETURNS void
AS $$
BEGIN
    INSERT INTO load.temps SELECT * FROM transformation.temps;
END;
$$ LANGUAGE plpgsql;

CALL insert_data(1, 2);

CREATE TABLE load.emplacement(
  magasin varchar,
  numDepartement integer,
  nomDepartement varchar,
  populationDepartement integer,
  typeRayonCourt char(1),
  typeRayonLong varchar,
  rayonBestsellers boolean,
  rayonRecents boolean,
  CONSTRAINT c_numDepartement CHECK (numDepartement IS NOT NULL),
  CONSTRAINT c_typeRayonCourt CHECK(typeRayonCourt='A' OR typeRayonCourt='E' OR typeRayonCourt='Y' OR typeRayonCourt='U'),
  CONSTRAINT c_typeRayonLong CHECK( typeRayonLong='Author' OR typeRayonLong='Editor' OR typeRayonLong='Year' OR typeRayonLong='UNKNOWN'),
  CONSTRAINT c_magasinFormat CHECK (magasin SIMILAR TO 'M(\d)+'),
  CONSTRAINT c_magasin PRIMARY KEY (magasin)
);

CREATE OR REPLACE FUNCTION insertDimensionEmplacement() RETURNS void
AS $$
BEGIN
    INSERT INTO load.emplacement SELECT * FROM transformation.emplacement;
END;
$$ LANGUAGE plpgsql;

CREATE TABLE load.livre(
   isbn varchar,
   titre varchar,
   auteurs varchar,
   langue varchar,
   datePublication date,
   editeur varchar,
   tags varchar,
   genre varchar,
   CONSTRAINT c_keyIsbn PRIMARY KEY (isbn)
);

CREATE OR REPLACE FUNCTION insertDimensionLivre() RETURNS void
AS $$
BEGIN
    INSERT INTO load.livre SELECT * FROM transformation.livre;
END;
$$ LANGUAGE plpgsql;

CREATE TABLE load.fait(
  numTicket varchar,
  dateVente date REFERENCES load.temps(dateVente),
  isbn varchar REFERENCES load.livre(isbn),
  magasin varchar REFERENCES load.emplacement(magasin),
  CONSTRAINT c_globalKey PRIMARY KEY (numTicket, dateVente, isbn, magasin)
);

CREATE OR REPLACE FUNCTION insertFait() RETURNS void
AS $$
BEGIN
    INSERT INTO load.fait SELECT * FROM transformation.fait;
END;
$$ LANGUAGE plpgsql;

