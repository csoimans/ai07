CREATE SCHEMA fbdt;
create table fbdt.departement(numDep integer, nomDep varchar, populationDep integer);

/* Cette table est supposée propre et donc on transfère directement les données*/

insert into fbdt.departement select
CAST(numeroDep as integer),CAST(nomDep as varchar),CAST(population as integer) from fbde.departement;
/* Cette table est supposée propre et donc on transfère directement les données*/
create table fbdt.marketing(
  magasin varchar,
  numDep integer,
  rayonnage char(1),
  bestseller boolean,
  recent boolean
);

insert into fbdt.marketing select
magasin,cast(numdepartement as integer), rayonnageabr,cast(bestseller as boolean),cast(recent as boolean) from fbde.marketing;


/* effacer la ligne de commentaire du fichier .ods quand elle a été récupérée dans l'extraction*/

delete from fbdt.marketing where length(magasin)>10;

  create table fbdt.caisse(
    numTicket varchar(12),
    dateVente date,
    isbn varchar(13),
    magasin varchar
  );

/* Vérifier la date */
   CREATE OR REPLACE FUNCTION checkDate(entryDate IN text) RETURNS date AS
   $$
   BEGIN
     IF entryDate SIMILAR TO '[\d]{4}-[\d]{2}-[\d]{2}' THEN
         return TO_DATE(entryDate, 'YYYY-MM-DD');
     ELSIF entryDate SIMILAR TO '[\d]{4}-[\d]{2}-[\d]{2}T[\d]{2}:[\d]{2}:[\d]{2}\+[\d]{2}:[\d]{2}' THEN
         return TO_DATE(entryDate, 'YYYY-MM-DD');
     ELSE
         return NULL;
     END IF;
   END;
   $$ LANGUAGE plpgsql;



   CREATE OR REPLACE FUNCTION checkNumberTicket(entryTicket IN text) RETURNS varchar(12) AS
   $$
   BEGIN
     IF entryTicket SIMILAR TO '[\d]{12}' THEN
         return CAST(entryTicket as varchar(12));
     ELSE
         return NULL;
     END IF;
   END;
   $$ LANGUAGE plpgsql;

   CREATE OR REPLACE FUNCTION checkMagasin(magasin IN text) RETURNS varchar AS
   $$
   BEGIN
     IF magasin SIMILAR TO 'M(\d)+' THEN
         return CAST(magasin as varchar);
     ELSE
         return NULL;
     END IF;
   END;
   $$ LANGUAGE plpgsql;

SELECT checkNumberTicket(numero), checkDate(date), checkNotNull(isbn), checkMagasin(store) from fbde.caisse WHERE
checkNumberTicket(numero) is not null and checkDate(date) is not null and checkNotNull(isbn) is not null and  checkMagasin(store) is not null;

SELECT count(*)  from fbde.caisse WHERE
checkNumberTicket(numero) is not null and checkDate(date) is not null and checkNotNull(isbn) is not null and  checkMagasin(store) is not null;
==> Il reste 453 072 entrées une fois viré ce qui sert à rien ==> 12% de perdu (512 018 lignes au départ)

insert into fbdt.caisse SELECT checkNumberTicket(numero), checkDate(date), checkNotNull(isbn), checkMagasin(store) from fbde.caisse;

/*   WHERE
checkNumberTicket(numero) is not null and checkDate(date) is not null and checkNotNull(isbn) is not null and  checkMagasin(store) is not null;
*/
create table fbdt.catalogue(
  isbn varchar(13),
  title varchar,
  authors varchar,
  language varchar,
  pubdate date,
  publisher varchar,
  genre varchar
);

insert into fbdt.catalogue select checkNotNull(isbn), checkNotNull(title), checkNotNull(authors), checkNotNull(language),
  checkDate(pubdate), checkNotNull(publisher), checkNotNull(genre) from fbde.catalogue;

alter table fbdt.catalogue ADD CONSTRAINT Con_Key_ISBN PRIMARY KEY (isbn);
alter table fbdt.marketing ADD CONSTRAINT Con_Key_Magasin PRIMARY KEY (magasin);

create table fbdt.dateDimensionTable as select distinct dateVente, TO_CHAR(dateVente, 'dd') as jour, TO_CHAR(dateVente, 'mm') as mois,
   TO_CHAR(dateVente, 'YYYY') as year from fbdt.caisse;

create view fbdt.emplacement as
select fbdt.marketing.magasin, fbdt.departement.numdep, fbdt.departement.nomdep,
fbdt.departement.populationdep, fbdt.marketing.rayonnage, fbdt.marketing.bestseller,
fbdt.marketing.recent from fbdt.marketing INNER JOIN fbdt.departement on fbdt.marketing.numdep= fbdt.departement.numdep;

create view fbdt.livre as
  select * from fbdt.catalogue;

create view fbdt.fait as
  select * from fbdt.caisse;

/*requête test*/
select fbdt.fait.magasin, count(fbdt.fait.numTicket), fbdt.emplacement.populationdep
from fbdt.fait inner join fbdt.emplacement on fbdt.fait.magasin = fbdt.emplacement.magasin group by fbdt.fait.magasin, fbdt.emplacement.populationdep;
/*
  Pour les rapports de vérification :
  Logger beaucoup. Utiliser min, max, moyenne pour les tests.
*/
