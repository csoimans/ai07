CREATE OR REPLACE FUNCTION checkDate(entryDate IN text) RETURNS date AS
$$
BEGIN
  IF entryDate SIMILAR TO '[\d]{4}-[\d]{2}-[\d]{2}' THEN
      return TO_DATE(entryDate, 'YYYY-MM-DD');
  ELSIF entryDate SIMILAR TO '[\d]{4}-[\d]{2}-[\d]{2}T[\d]{2}:[\d]{2}:[\d]{2}\+[\d]{2}:[\d]{2}' THEN
      return TO_DATE(entryDate, 'YYYY-MM-DD');
  ELSE
      return NULL;
  END IF;
END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION checkNumberTicket(entryTicket IN text) RETURNS varchar(12) AS
$$
BEGIN
  IF entryTicket SIMILAR TO '[\d]{12}' THEN
      return CAST(entryTicket as varchar(12));
  ELSE
      return NULL;
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION checkMagasin(magasin IN text) RETURNS varchar AS
$$
BEGIN
  IF magasin SIMILAR TO 'M(\d)+' THEN
      return CAST(magasin as varchar);
  ELSE
      return NULL;
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION checkNotNull(textToCheck IN text) RETURNS varchar AS
$$
BEGIN
  IF textToCheck SIMILAR TO ' *' THEN
      return NULL;
  ELSE
      return cast(textToCheck as varchar);
  END IF;
END;
$$ LANGUAGE plpgsql;
