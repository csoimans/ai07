/*
Table des départements :
En observant la table d'extraction, on constate qu'il n'y a pas d'erreurs,
ni dans le format (un cast même implicite suffit à faire passer les données dans le bon format),
ni de données perdues (aucune données nulles).
Il y a peu de chances que le format de ces données soit modifiées ou qu'elles contiennent des erreurs.
(ça semble être des données officielles).
On peut créer la table de transformation simplement en ajoutant les contraintes de format.
On vérifiera par la suite que toutes les données sont bien récupérées.
*/

CREATE SCHEMA transformation;

CREATE TABLE transformation.departement(
    numDepartement INTEGER,
    nomDepartement VARCHAR,
    populationDepartement INTEGER
);

INSERT INTO transformation.departement
	SELECT CAST(numDepartement AS INTEGER),CAST(nomDepartement AS VARCHAR),
	CAST(populationDepartement AS INTEGER) FROM extraction.departement;

/*
Table des magasins :
En observant la table d'extraction, on constate qu'il n'y a pas d'erreurs,
ni dans le format (un cast même implicite suffit à faire passer les données dans le bon format),
ni de données perdues (aucune données nulles).
En revanche, il y a une ligne de commentaires à la fin du document qu'il faudra évincer
pendant le traitement des données récupérées.
On peut créer la table de transformation simplement en ajoutant les contraintes de format
et de non nullité. On vérifiera par la suite que toutes les données sont bien récupérées.
*/

CREATE TABLE transformation.magasin(
	magasin VARCHAR,
	numDepartement INTEGER,
	typeRayonCourt CHAR(1),
	typeRayonLong VARCHAR,
	rayonBestsellers BOOLEAN,
	rayonRecents BOOLEAN
);

INSERT INTO transformation.magasin
	SELECT magasin, CAST(numDepartement AS INTEGER), typeRayonCourt, typeRayonLong,
	CAST(rayonBestsellers AS BOOLEAN), CAST(rayonRecents AS BOOLEAN) from extraction.magasin;

/*
On gère ici la ligne de commentaires, sachant que la taille des magasins est limitée étant donné leur format.
*/
DELETE FROM transformation.magasin WHERE LENGTH(magasin)>10;
/*
Table des livres :
Le catalogue lui a des données parfois hasardeuses en terme de contenu (des ISBN qui ne sont pas au
bon nombre de caractères par exemple) mais pas de gros problèmes au niveau du format. On va donc
récupérer les données dans les bons formats, mais sans poser de contraintes précises, de tailles ou
ce genre de choses. La base étant une base Oracle, les dates sont normalement soit nulles, soit renseignées
dans un bon format. De façon à être sûrs de ne pas perdre d'information, on conservera la date au format
varchar jusqu'au moment de la passer dans les vues.
*/

CREATE TABLE transformation.catalogue(
 	reference VARCHAR,
	isbn VARCHAR,
	titre VARCHAR,
	auteurs VARCHAR,
	langue VARCHAR,
	datePublication VARCHAR,
	editeur VARCHAR,
	tags VARCHAR,
	genre VARCHAR
);

INSERT INTO transformation.catalogue SELECT reference, isbn, titre, auteurs,
  langue, datePublication, editeur, tags, genre from extraction.catalogue;

/*
Table des tickets de caisse :
Cette table par contre a beaucoup de problèmes du point de vue des formats (date perdues,
lettre M du magasin perdue ...). On peut sans risque de perdre des informations passer en VARCHAR
les données (sous réserve de ne pas imposer de longueur). On ne mettra aucune contrainte de non nullité
avant la transformation.
*/

CREATE TABLE transformation.caisse(
	numTicket VARCHAR,
	dateVente VARCHAR,
	isbn VARCHAR,
	magasin VARCHAR
);

INSERT INTO transformation.caisse SELECT numTicket, dateVente, isbn, magasin from extraction.caisse;

/*
A ce stade, on a normalement récupéré toutes nos données de la table d'extraction.
On le vérifie en réutilisant les requêtes des tests pour l'extraction, en vérifiant en plus que l'on a
bien le même nombre de lignes avec éléments nuls.
*/


/*
On va commencer à traiter les informations que l'on a récupéré. Afin de gérer au mieux les entrées ou
une valeur possiblement clé étrangère plus tard sera nulle, on crée une entrée magique correspondant
à l'entrée inconnue pour chaque valeur pouvant poser problème pour servir de clé.
*/

CREATE OR REPLACE FUNCTION checkDate(entryDate IN text) RETURNS date AS
$$
BEGIN
  IF entryDate SIMILAR TO '[\d]{4}-[\d]{2}-[\d]{2}' THEN
      return TO_DATE(entryDate, 'YYYY-MM-DD');
  ELSIF entryDate SIMILAR TO '[\d]{4}-[\d]{2}-[\d]{2}T[\d]{2}:[\d]{2}:[\d]{2}\+[\d]{2}:[\d]{2}' THEN
      return TO_DATE(entryDate, 'YYYY-MM-DD');
  ELSE
      return TO_DATE('0000-00-00', 'YYYY-MM-DD');
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION checkNumberTicket(entryTicket IN text) RETURNS varchar AS
$$
BEGIN
  IF entryTicket SIMILAR TO '[\d]{12}' THEN
      return CAST(entryTicket AS VARCHAR);
  ELSE
      return CAST('000000000000' AS VARCHAR);
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION checkMagasin(magasin IN text) RETURNS varchar AS
$$
BEGIN
  IF magasin SIMILAR TO 'M(\d)+' THEN
      return CAST(magasin AS VARCHAR);
  ELSE
      return CAST('M0' AS VARCHAR);
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION checkNotNull(textToCheck IN text) RETURNS varchar AS
$$
BEGIN
  IF textToCheck SIMILAR TO ' *' THEN
      return CAST('UNKNOWN' AS VARCHAR);
  ELSIF textToCheck='?' THEN
      return CAST('UNKNOWN' AS VARCHAR);
  ELSE
      return cast(textToCheck as varchar);
  END IF;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION getDatePart(dateOriginal IN date, datePartNeeded in text) RETURNS integer AS
$$
BEGIN
RETURN CASE  WHEN datePartNeeded='day' THEN extract(day from dateOriginal)
                    WHEN datePartNeeded='month' THEN extract(month from dateOriginal)
                    WHEN datePartNeeded='isoyear' THEN extract(isoyear from dateOriginal)
                    WHEN datePartNeeded='quarter' THEN extract(quarter from dateOriginal)
                    WHEN datePartNeeded='week' THEN extract(week from dateOriginal)
                    WHEN datePartNeeded='isodow' THEN extract(isodow from dateOriginal)
                    ELSE 0 END;
END;
$$ LANGUAGE plpgsql;
/*
On ajoute les entrées inconnues dans les tables de dimension.
*/
INSERT INTO transformation.magasin VALUES ('M0', 0, 'U','UNKNOWN', NULL, NULL);
INSERT INTO transformation.departement VALUES (0, 'UNKNOWN', 0);
INSERT INTO transformation.catalogue VALUES (0, 'UNKNOWN', 'UNKNOWN','UNKNOWN','UNKNOWN',
	TO_DATE('0000-00-00', 'YYYY-MM-DD'), 'UNKNOWN','UNKNOWN','UNKNOWN');


/*
De cette façon on corrige en entrée dans les vues toutes les données qui nous intéressent.
Pour gérer les dates, on crée une vue matérialisée à partir des données de dates.
*/

create table transformation.dateDimensionTable as select distinct checkDate(dateVente) as dateVente, getDatePart(checkDate(dateVente), 'day') as jour,
	getDatePart(checkDate(dateVente), 'month') as mois, getDatePart(checkDate(dateVente), 'isoyear') as annee, getDatePart(checkDate(dateVente), 'quarter') as saison,
	getDatePart(checkDate(dateVente), 'week') as semaine, getDatePart(checkDate(dateVente), 'isodow') as jourSemaine from transformation.caisse;

/*
A partir de là, créer la vue de la dimension "Emplacement" est possible.
Quelles sont les contraintes de cette table ?
- Les magasins sont de la forme 'M[\d]*' et leur valeur est obligatoirement renseignée (ils seront clés étrangère pour la table des faits)
- Les types de rayonnages sont toujours 'A','E' ou 'Y' en abrégés, 'Author', 'Editor' ou 'Year' en mot complet.
- Tout champ ayant une valeur inconnue comporte la valeur 'UNKNOWN'

On créé la vue avec les contraintes :
*/
CREATE OR REPLACE VIEW transformation.emplacement AS
SELECT DISTINCT checkMagasin(magasin) as magasin, departement.numDepartement, nomDepartement, populationDepartement,
 checkNotNull(typeRayonCourt) as typeRayonCourt, checkNotNull(typeRayonLong) as typeRayonLong, rayonBestsellers,
  rayonRecents from transformation.magasin
 INNER JOIN transformation.departement ON magasin.numDepartement = departement.numDepartement;



CREATE OR REPLACE VIEW transformation.temps AS SELECT DISTINCT dateVente, jour, mois, annee, saison, semaine, jourSemaine
from transformation.dateDimensionTable;

CREATE OR REPLACE VIEW transformation.livre AS SELECT DISTINCT checkNotNull(isbn) as isbn, checkNotNull(titre) as titre,
 checkNotNull(auteurs) as auteurs, langue, checkDate(datePublication) as datePublication, checkNotNull(editeur)
 as editeur, tags, genre
 FROM transformation.catalogue;

CREATE OR REPLACE VIEW transformation.fait AS SELECT checkNumberTicket(numTicket) as numTicket, checkDate(dateVente) as dateVente,
 checkNotNull(isbn) as isbn, checkMagasin(magasin) as magasin FROM transformation.caisse;


UPDATE transformation.caisse
SET isbn = 'UNKNOWN'
WHERE isbn in (SELECT DISTINCT isbn from transformation.caisse A where
not exists (select isbn from transformation.catalogue B where A.isbn = B.isbn));

UPDATE transformation.caisse
SET magasin = 'M0'
WHERE magasin in (SELECT DISTINCT magasin from transformation.caisse A where
not exists (select magasin from transformation.magasin B where A.magasin = B.magasin));

UPDATE transformation.caisse
SET dateVente = checkDate(dateVente)
WHERE true;
