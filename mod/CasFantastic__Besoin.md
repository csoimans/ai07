# Cas Fantastic : analyse du besoin

## Avant propos
L'information qui les intéresse est toujours le nombre de livres vendus. Ce qui varie en revanche est la dimension à laquelle la direction s'intéresse pour analyser les chiffres de ventes.

Les interrogations seront du type "Quelle est l'influence de " + dimension + "sur le volume des ventes (nombre de livres vendus) ?".
Dans les prochaines sections, je donnerai simplement les dimensions à placer dans cette question.

## Besoin de la direction marketing

La direction ne s'intéresse pas à un livre en particulier ici mais à tous les livres en même temps. Les dimensions sont :

* Une période de temps : jour de la semaine, mois de l'année, saison de l'année 

```
nombre de ventes de livres
/ période de temps (jour, mois, saison/trimestre)
```
* L'organisation du rayonnage : présence de rayon best seller ou non, de rayon récent ou non, organisation des livres par éditeur, par auteur ou par année
```
nombre de ventes de livres
/ type de rayonnage
/ présence d'un rayon "bestsellers"
/ présence d'un rayon "récents"
```
* Par emplacement géographique : département du magasin, emplacement du magasin
```
nombre de ventes de livres
/ magasin
/ emplacement de magasin (département)
/ population du département du magasin
```
## Besoin de la direction éditoriale

La direction s'intéresse ici parfois aux ventes d'un livre en particulier (en faisant varier l'édition par exemple) ou à tous les livres. Je préciserai "un livre en particulier" dans le premier cas. Les dimensions sont :

* Par emplacement géographique / date
```
nombre de ventes d'un livre en particulier
/ magasin
/ emplacement de magasin (département)
/ population du département du magasin
/ moment (jour, mois, année, saison)
```
* Auteur, éditeur, date de parution

```
nombre de ventes de livres
/ auteur
/ éditeur
/ année de parution

```
* L'organisation du rayonnage : présence de rayon best seller ou non, de rayon récent ou non, organisation des livres par éditeur, par auteur ou par année
```
nombre de ventes de livres
/ type de rayonnage
/ présence d'un rayon "bestsellers"
/ présence d'un rayon "récents"
```
* Corrélation entre une date et la vente d'un livre ancien
```
nombre de ventes de livres
/ date de parution
/ date de vente (jour, mois, année, saison)
```