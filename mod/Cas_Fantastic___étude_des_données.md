# Cas Fantastic : étude des données

## Avant propos

## Source 1 : Tickets de caisse
#### Présentation
Nom : Fantastic.txt
Format : extension .txt mais fichier CSV dans sa syntaxe
Nombre de lignes : 512 019

Le fichier contient normalement 4 entrées par ligne :
* Le numéro du ticket de caisse, sous forme de chaine de caractères
* La date de vente, sous format date directement (pas de chaine de caractères) mais selon des patrons de dates variables (YYYY-MM-JJ ou YY-MM-JJ ...)
* ISBN du produit, sous forme de chaine de caractères
* Identification du magasin, avec un code magasin 'MX'où X est le numéro du magasin

#### Qualité globale des données : **2**

- Certains tickets de caisses ont "0" comme numéro de ticket. Une vente de plusieurs livres au même moment est représentée par un même numéro de ticket. En revanche, il n'y a jamais deux fois le même numéro de ticket pour une vente ayant eu lieu un autre jour.
- Comme dit précédemment, les dates ne sont pas toutes au même format et nécéssiterait une uniformisation
- Parfois un des champs est complètement vide : il manque la date ou l'ISBN par exemple
- Il y a quelquefois eu des erreurs de frappe : M manquant devant le numéro du magasin, année de la date perdue mais le tiret indique qu'elle avait probablement été saisie au complet ...
- Les ISBN ne sont pas forcément aux normes (ISBN beaucoup trop court) : cependant en recoupant avec les autres sources, on constate que ce problème se trouve aussi au niveau du catalogue, il est probable que les erreurs d'ISBN se recoupent donc entre le catalogue et ce fichier.


| Colonne |  Qualité  |
| -------- | -------- |
| N° de Ticket     | 3|
| Date    | 2     |
|   ISBN   | 2     |
| Magasin     | 2     |

Ces données sont exploitables mais demanderont une identification de tous les cas d'anomalie dans l'ETL pour limiter les erreurs. Une fois les anomalies corrigeable réglées, les erreurs restantes ne devraient pas être trop impactantes dans les statistiques dans la mesure où l'on a quand même plus de 500 000 lignes de données.

#### Utilité globale des données : **3**

Ces données sont la base de tous les besoins des services dans la mesure où elles sont les seules à permettre d'obtenir le nombre des ventes
- Le numéro de ticket nous permet d'identifier le nombre de passage en caisse et de regrouper les livres achetés en même temps ; ça peut être intéressant pour répondre à une question qu'on ne nous a pas encore posée, par exemple "quand on achète plusieurs livres, combien sont du même auteur, du même éditeur, proviennent du même rayon", mais le nombre de ventes peut aussi être obtenu avec le nombre de lignes.
- La date va permettre de classer les ventes selon le jour, la période de l'année ou la saison
- L'ISBN est une information liée au catalogue mais est plutôt une donnée opérationnelle. Non seulement l'ISBN comporte des anomalies mais il n'a pas d'intérêt du point de vue décisionnel
- Le numéro du magasin va permettre de recouper avec le fichier des magasins et de retrouver les emplacements géographiques

| Colonne |  Utilité  |
| -------- | -------- |
| N° de Ticket     | 2|
| Date    | 3     |
|   ISBN   | 0     |
| Magasin     | 3     |


## Source 2 : Magasins
#### Présentation
Nom : marketing.ods
Format : fichier .ods, type tableur
Nombre de lignes : 152

Le fichier contient normalement 5 entrées par ligne :
* Numéro du magasin, chaine de caractères sous la forme "MX" avec X le numéro du magasin
* Le numéro du département, nombre
* L'organisation des rayons, représenté soit par une lettre soit par le nom complet, un parmi l'énumération {"Editor", "Author", "Year"}
* Présence d'un rayon "bestseller", booléen 0-1
* Présence d'un rayon "récent", booléen 0-1

#### Qualité globale des données : **3**

Il n'y a pas d'anomalies dans les données, aucune faute de frappe, aucune information non renseignée.


| Colonne |  Qualité  |
| -------- | -------- |
|   Magasin   | 3|
|   Département  | 3     |
|   Rayonnage   | 3     |
|    Rayon Bestseller  | 3     |
|   Rayon Récents   | 3     |


#### Utilité globale des données : **3**

Ces données sont nécessaires pour répondre à toutes les questions concernant l'emplacement géographique et l'influence du rayonnage et de la présence de rayon particulier.
- Sans la colonne indiquant le numéro du magasin, on ne peut pas recouper les informations de magasin sur les tickets de caisse.
- Sans la colonne des départements, on ne peut pas relier un magasin et son emplacement géographique.
- Sans les informations des 3 dernières colonnes sur les rayons et leur organisation, on a aucune information pour relier l'organisation des rayons et les ventes retrouvées grâce au magasin.

| Colonne |  Utilité  |
| -------- | -------- |
|   Magasin   | 3|
|   Département  | 3     |
|   Rayonnage   | 3     |
|    Rayon Bestseller  | 3     |
|   Rayon Récents   | 3     |
## Source 3 : Catalogue des livres

#### Présentation
Nom : catalogue
Format : base de données sql
Nombre de lignes : 1443

La base a 9 colonnes :
* Référence : un genre de clé, type nombre
* ISBN : chaine de caractères de maximum 13 chiffres
* Titre : chaine de caractères
* Auteur : chaine de caractères contenant un ou plusieurs auteurs dans la liste
* Langage : chaine de caractères
* Date de publication : chaine de caractères
* Éditeur : chaine de caractères
* Tags : chaine de caractères, liste des mots clés associés au livre
* Genre : chaine de caractères, genre littéraire du livre

#### Qualité globale des données : **2**

La base contient quelques anomalies :
- Parfois la langue n'est pas renseignée
- L'ISBN a une longueur variable au lieu d'être fixé à 13 caractères

En revanche, la manière dont les données sont stockées rend difficile leur exploitation :
- Les auteurs quand ils sont plusieurs sont mis les uns derrière les autres avec des caractères '&'
- Si le livre appartient à une saga, on trouvera le nom de celle ci entre crochets dans le titre. Les tags sont mis à la suite dans la même colonne. En fait certaines colonnes plusieurs informations en même temps qu'il serait utile de séparer pour isoler celle qui nous intéresse vraiment, le titre pour un livre (les tags sont liés à l'opérationnels et il n'est probablement pas nécéssaire de les conserver)
- Les dates de parutions sont des Timestamp alors que la date en YYYY-MM-JJ suffisait
- Il y a des doublons dans le catalogue : un même livre, même édition, même titre, même auteur, même date de parution mais ISBN différents et référence différente ("Journal d'un vampire" par exemple)



| Colonne |  Qualité  |
| -------- | -------- |
|   Référence   | 3|
|   ISBN  | 2     |
|    Titre  | 2     |
|    Auteurs  | 2     |
|    Éditeur  | 2     |
|    Langage  | 2     |
|    Date de publication  | 2     |
|    Tags  | 2     |
|    Genre  | 2     |



#### Utilité globale des données : **2**

En fait seules certaines de ces données sont utiles pour la réflexion décisionnelle :
- Les titres des livres lorsque l'on veut on s'intéresser aux ventes d'un livre en particulier
- La date de parution pour traiter l'influence de l'ancienneté du livre
- L'édition et les auteurs pour vérifier leur influence sur la vente
- La langue et le genre, bien qu'il ne nous soit pas demandé explicitement de les traiter, seront probablement des candidats possibles comme facteur influençant les ventes, il est donc utile de les conserver
- L'ISBN est nécessaire pour retrouver un livre à partir du ticket de caisse et fait le lien entre ce ticket et toutes les données du livre
- Le reste n'a d'intérêt que pour l'opérationnel et ne servira pas pour nos usages 

| Colonne |  Utilité  |
| -------- | -------- |
|   Référence   | 0|
|   ISBN  | 3     |
|    Titre  | 2     |
|    Auteurs  | 3     |
|    Éditeur  | 3     |
|    Langage  | 1     |
|    Date de publication  | 3     |
|    Tags  | 0     |
|    Genre  | 1     |
## Source 4 : Données sur les départements

#### Présentation
Nom : departementInsee2003.txt
Format : fichier .txt à priori CSV en contenu
Nombre de lignes : 95

Le fichier contient normalement 3 entrées par ligne :
* Numéro du département : chiffres dans une chaine de caractères, toujours sur deux caractères
* Nom du département : chaine de caractères
* Nombres d'habitants du département en 2003 : chiffres passés dans une chaine de caractères

#### Qualité globale des données : **3**

Il n'y a pas d'anomalies dans les données, aucune faute de frappe, aucune information non renseignée.
La seule chose qui pourrait éventuellement demander un peu de traitement serait la conversion des chaines de caractères qui devraient être des nombres de façon à coller aux informations fournies par le marketing.


| Colonne |  Qualité  |
| -------- | -------- |
|   Département   | 3|
|   Nom du département  | 3     |
|   Nombre d'habitants   | 3     |



#### Utilité globale des données : **2**

Ces données sont relativement anciennes et apporte juste l'information du nombre d'habitants dans un département, rien sur les tranches d'âges par exemple. Il sert simplement à associer un nom de département à son numéro, ce qui est une information publique ne nécéssitant pas forcément de banque de données de départ.

| Colonne |  Utilité  |
| -------- | -------- |
|   Département   | 2|
|   Nom du département  | 1     |
|   Nombre d'habitants   | 1     |